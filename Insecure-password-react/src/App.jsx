import React, { Component} from 'react';
import './App.scss';
import {Router ,Route} from "react-router-dom";
import {Home} from "./components/Home";
import {Accounts} from "./components/accounts/accounts";
import history from "./history";
import {Apikey} from "./components/apikeys/apikey";
import {Password} from "./components/passwords/password";
import {Logout} from "./components/logout";
import {View_passwords} from "./components/passwords/view_passwords";



export class App extends Component{

    render() {
        return(
                <Router history={history}>
                <Route exact path="/" component={Home}/>
                <Route path="/accounts" component={Accounts}/>
                <Route path="/apikey" component={Apikey}/>
                <Route path="/passwords" component={Password}/>
                <Route path="/logout" component={Logout}/>
                <Route path="/api/passwords" component={View_passwords}/>
            </Router>
        );
    }
}
export default App;
