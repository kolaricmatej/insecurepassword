import React, {Component} from "react";
import {BrowserRouter, Route} from "react-router-dom";
import {Login, Registration} from "./login";
import {Accounts} from "./accounts/accounts";



export class Home extends Component{
constructor(props) {
    super(props);
    this.state={
      isLoginActive: true,
    };
  }


    componentDidMount() {
    this.rightSide.classList.add("right");
    }

    changeState(){
    const{isLoginActive}=this.state;

    if(isLoginActive){
      this.rightSide.classList.remove("right");
      this.rightSide.classList.add("left");
    }else{
      this.rightSide.classList.remove("left");
      this.rightSide.classList.add("right");
    }
    this.setState(prevState=>({isLoginActive: !prevState.isLoginActive}));
  }

  render(){
    const { isLoginActive } = this.state;
    const current=isLoginActive ? "Registration" : "Login";
    const currentActive=isLoginActive ? "login" : "registration";
    return (
        <BrowserRouter>
          <Route path="/accounts" component={Accounts}/>
              <div className="App">
           <div className="login">
             <div className="container" ref={ref => (this.container = ref)}>
            {isLoginActive && (<Login  containerRef={(ref) => this.current = ref}/>)}
            {!isLoginActive && (<Registration containerRef={(ref) => this.current = ref}/>)}
             </div>
          <RightSide id="rightSide" current={current} currentActive={currentActive} containerRef={ref=>this.rightSide=ref} onClick={this.changeState.bind(this)}/>
        </div>
      </div>
        </BrowserRouter>

    );
  }
}

export const RightSide = props => {
  return <div className="right-side" ref={props.containerRef}  onClick={props.onClick}>
          <div className="inner-container">
          <div className="text">{props.current}</div>
      </div>
    </div>
}
