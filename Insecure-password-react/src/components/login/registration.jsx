import loginImg from "./login.svg";
import React, {Component} from "react";
import {registration} from "../../functions/userFucntion";

import {
    checkRegistration,
    checkEmail,
    checkPassword,
    SuccessRegistration,
    ErrorRegistration, ErrorEmailExists
} from "../../functions/messages";
import history from "../../history";

export class Registration extends Component{
   constructor(props) {
        super(props);
        this.state={
            firstName:"",
            lastName:"",
            email:"",
            password:"",
            repeat:"",
            registrationErrors:{}
        };
        this.onSubmit=this.onSubmit.bind(this);
        this.onChange=this.onChange.bind(this);
    }
    onChange(event){
        this.setState({[event.target.name]: event.target.value});
    }
    onSubmit(event){
        event.preventDefault()
        const newUser={
            email:this.state.email,
            password:this.state.password,
            firstName:this.state.firstName,
            lastName:this.state.lastName,
        }
        registration(newUser).then(res=>{
           if(res[1]===200){
            SuccessRegistration()
              window.location.reload();
           }else if(res[1]===409 || res[1]===500){
            ErrorEmailExists()
           }
        }).catch(err=>{
            ErrorRegistration()
        })
    }


    render(){
        return(
            <div className="base-container" ref={this.props.containerRef}>
                <div className="header">Registration</div>
                <div className="content">
                    <div className="image">
                        <img src={loginImg}/>
                    </div>

            <div className="form">
                <form noValidate onSubmit={this.onSubmit} >
                <div className="form-group">
                    <input id="firstName" type="text" name="firstName"  placeholder="e.g. John" value={this.state.firstName} onChange={this.onChange}/>
                     <label htmlFor="firstName"> First name</label>
                </div>
                <div className="form-group">
                    <input id="lastName" type="text" name="lastName" placeholder="e.g. Smith" value={this.state.lastName}  onChange={this.onChange}/>
                     <label htmlFor="lastName"> Last name</label>
                </div>
                <div className="form-group">
                    <input className="form-field" id="registration-email" type="email" name="email" placeholder="Insert your email"  onKeyUp={checkEmail} value={this.state.email}  onChange={this.onChange}  required/>
                     <label className="form-label" for="registration-email" htmlFor="email"> Email</label>
                </div>
                <div className="form-group">
                    <input className="form-field" id="registration-password" type="password" name="password" placeholder="Insert password" value={this.state.password} onChange={this.onChange} onKeyUp={checkPassword} required/>
                   <label className="form-label" for="registration-password" htmlFor="password">Password</label>
                </div>
                <div className="form-group">
                    <input className="form-field" id="registration-repeat" type="password" name="repeat"  onKeyUp={checkPassword} placeholder="Insert password" value={this.state.repeat}  onChange={this.onChange} required/>
                    <label className="form-label" for="registration-repeat" htmlFor="repeat">Repeat password</label>
                </div>
                    <button className="btn" id="registration-btn" onClick={checkRegistration}  type="submit">Registration</button>
                </form>
                    </div>
                </div>
                <div className="footer">
                    <div className="message-dialog message-dialog-hide desktop-top-margin-big">
                    <div className="message-dialog-title">
                    </div>
                    <p className="message-dialog-content">
                    </p>
                </div>
            </div>
                </div>
        );
    }
}

