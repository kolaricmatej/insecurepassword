import React, {Component} from "react";
import loginImg from './proba.png'
import { login} from "../../functions/userFucntion";

import {checkValidation, incorrectLogin, loginCookie, successLogin} from "../../functions/messages";
import history from "../../history";


export class Login extends Component {
    constructor(props) {
        super(props);
        this.state={
            email:"",
            password:"",
            isChecked: true,
            loginErrors:{}
        };
        this.onSubmit=this.onSubmit.bind(this);
        this.onChange=this.onChange.bind(this);
        this.onLoad=this.onLoad.bind(this);

    }

    toggleChange = () => {
    this.setState({
      isChecked: !this.state.isChecked,
    });
  }
  componentDidMount() {
        loginCookie()
        this.onLoad()
  }

    onLoad(){
        if(localStorage.getItem("username")!=="" &&localStorage.getItem("password")){
            this.state.email=localStorage.getItem("username");
            this.state.password=localStorage.getItem("password");
        }

    }
    onChange(event){
        this.setState({[event.target.name]: event.target.value});
    }
    onSubmit(event){
        event.preventDefault();
        if(this.state.isChecked==true){
            localStorage.setItem("username",document.getElementById("login-email").value);
            localStorage.setItem("password",document.getElementById("login-password").value);
        }
        const user={
            email:this.state.email,
            password:this.state.password
        }
        login(user).then(res=>{
            if(res[1]===200){
              successLogin();
              history.push('/accounts')
            }else{
               incorrectLogin()
            }
        }).catch(err=>{
            incorrectLogin()
        })
    }
    render(){
        return(

            <div className="base-container" ref={this.props.containerRef}>
                <div className="header"  id="login">Login</div>
                <div className="content">
                    <div className="image">
                        <img src={loginImg}/>
                    </div>
                    <div className="form">
                        <form noValidate onSubmit={this.onSubmit} onLoad={this.onLoad}>
                        <div className="form-group">
                            <input className="form-field" id="login-email" type="email" name="email"
                                   placeholder="Insert your email" value={this.state.email} onChange={this.onChange}
                                   required/>
                            <label className="form-label" htmlFor="login-email"> Email</label>

                        </div>
                        <div className="form-group">
                            <input className="form-field" id="login-password" type="password" name="password" placeholder="Insert password"  value={this.state.password} onChange={this.onChange} required/>
                            <label className="form-label" htmlFor="login-password">Password</label>
                        </div>
                            <div>
                            <label className="form-label" htmlFor="login-remember">Remember me?</label>
                            <input id="login-remember" className="form-field" type="checkbox" checked={this.state.isChecked} onChange={this.toggleChange}/>
                            </div>
                             <button style={{marginTop:"10%"}} id="login-submit" type="submit" onClick={checkValidation}  className="btn">Login</button>
                        </form>
                    </div>
                </div>
                <div className="footer">
                    <div className="message-dialog message-dialog-hide desktop-top-margin-big">
                    <div className="message-dialog-title">
                    </div>
                    <p className="message-dialog-content">
                    </p>
                </div>
            </div>
            </div>
        );
    }
}
export default Login