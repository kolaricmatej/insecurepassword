import React, {Component} from "react";
import axios from "axios";
import history from "../history";
import {LogoutMessage} from "../functions/messages";
import jwt_decode from "jwt-decode"
import {refreshToken} from "../functions/userFucntion";

export class Logout extends Component{
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.logoutAccess();
        this.logoutRefresh();
        LogoutMessage();
        setTimeout(this.redirectToLogin,3000);
    }
    redirectToLogin(){
        history.push("/")
    }

    async logoutAccess(){
        var token=localStorage.getItem("access_token");
        var decoded=jwt_decode(token);
        var time_exp=decoded.exp;
        if(time_exp<new Date().getTime()/1000){
          await refreshToken();
        axios.get("http://localhost:5000/ipm/api/v0/logout/access_revoke",{headers: {
    'Authorization': `Bearer ${localStorage.getItem("access_token")}`
        }},).then(res=>{
            return console.log(res);
        })
        }
    }

    async logoutRefresh(){
        var token=localStorage.getItem("access_token");
        var decoded=jwt_decode(token);
        var time_exp=decoded.exp;
        if(time_exp<new Date().getTime()/1000) {
            await refreshToken();
        }
          axios.get("http://localhost:5000/ipm/api/v0/logout/refresh_revoke", {
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem("refresh_token")}`
                }
            },).then(res => {
                return console.log(res);
            })
    }

    render() {
        return(
        <div style={{position:"sticky"}} className="message-dialog message-dialog-hide desktop-top-margin-big">
            <div className="message-dialog-title">
            </div>
            <p className="message-dialog-content"></p>
        </div>
        );
    }
}
export default Logout;