import React, {Component} from "react";
import {NavigationBar} from "../navigation/navigationBar";
import {Add_apikey} from "../apikeys/add_apikey";
import {View_apikey} from "../apikeys/view_apikey";



export class Apikey extends Component{
  constructor(props) {
    super(props);
    this.state={
      viewAll: true,
    };
  }

   componentDidMount() {
    this.rightSide.classList.add("right");
    }

  changeState(){
    const{viewAll}=this.state;

    if(viewAll){
      this.rightSide.classList.remove("right");
      this.rightSide.classList.add("left");
    }else{
      this.rightSide.classList.remove("left");
      this.rightSide.classList.add("right");
    }
    this.setState(prevState=>({viewAll: !prevState.viewAll}));
  }

  render(){
     let links = [
      { label: 'Accounts', link: '/accounts'},
      { label: 'API key', link: '/apikey',active:true },
      { label: 'Passwords', link: '/passwords' },
      { label: 'Log out', link: '/logout' },
    ];

    const { viewAll } = this.state;
    const current=viewAll ? "Add new API keys" : "View API key";
    const currentActive=viewAll ? "add" : "view";
    return (
          <div className="App">
              <div id="navigation" className="container-center">
                 <NavigationBar links={links}  />
                </div>
           <div className="account">
             <div className="container" ref={ref => (this.container = ref)}>
            {viewAll && (<View_apikey containerRef={(ref) => this.current = ref}/>)}
            {!viewAll && (<Add_apikey containerRef={(ref) => this.current = ref}/>)}
             </div>
          <RightSide id="rightSide" current={current} currentActive={currentActive} containerRef={ref=>this.rightSide=ref} onClick={this.changeState.bind(this)}/>
        </div>
      </div>

    );
  }
}

export const RightSide = props => {
  return <div className="right-side" ref={props.containerRef} onClick={props.onClick}>
          <div className="inner-container">
          <div className="text">{props.current}</div>
      </div>
    </div>
}
export default Apikey;