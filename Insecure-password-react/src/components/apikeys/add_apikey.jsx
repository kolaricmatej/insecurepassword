import React, {Component} from 'react';
import "../accounts/style_acc.scss";
import "../../App.scss";
import keyImg from "./add_api.svg";
import {add_apikey} from "../../functions/userFucntion";

import { ErrorApiKey, SecretKeyMessage} from "../../functions/messages";

export class Add_apikey extends Component {
    constructor(props) {
        super(props);
        this.state = {
            key: "",
            secret: "",
            last_use: ""
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }

      onChange(event){
        this.setState({[event.target.name]: event.target.value});
    }

    onSubmit(event){
        event.preventDefault()
        add_apikey().then(res=>{
            try{
                if(res[1]===200) {
                document.getElementById("apikey-public").value= res[0]["key"];
               document.getElementById("apikey-created").value = res[0]["date"];
                this.state.secret=res[0]["secret"];
                SecretKeyMessage(this.state.secret);
            }else{
                ErrorApiKey()
            }
            }catch (e) {
                ErrorApiKey()
            }
        })
    }

     render() {
        return (
            <div className="base-container" ref={this.props.containerRef}>
                    <div className="header" id="login">Add API key</div>
                    <div className="content">
                        <div className="image">
                            <img src={keyImg}/>
                        </div>
                        <div className="form">
                            <form noValidate >
                                <div className="form-group">
                                    <input className="form-field" id="apikey-public" type="text" name="key"
                                           placeholder="Your API key" value="" />
                                    <label className="form-label" htmlFor="apikey-public"> Public password</label>
                                </div>
                                <div className="form-group">
                                    <input className="form-field" id="apikey-created" type="text" name="created"
                                           placeholder="Created at: " value=""/>
                                    <label className="form-label" htmlFor="password">Created at</label>
                                </div>
                                  <button id="apikey-submit" type="submit" onClick={this.onSubmit}
                                        className="btn">Generate API key</button>
                            </form>
                        </div>
                    </div>
                    <div className="footer">
                        <div className="message-dialog message-dialog-hide desktop-top-margin-big">
                            <div className="message-dialog-title">
                            </div>
                            <p className="message-dialog-content">
                            </p>
                        </div>
                    </div>
            </div>
        );
    }
}
export default Add_apikey;