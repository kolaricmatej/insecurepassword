import React, {Component} from "react";
import apiImg from "./view_api.svg";

import "../accounts/all_acc_style.scss";
import "../slider.css";
import Slider from "react-animated-slider";
import axios from "axios";
import {
    ErrorDeleteAPIkey,
    SuccessDeleteAPIkey,
} from "../../functions/messages";
import jwt_decode from "jwt-decode";
import {refreshToken} from "../../functions/userFucntion";



export class View_apikey extends Component{
        constructor(props) {
        super(props);
        this.state = {
            api_id:"",
            api_key: "",
            secret: "",
            created: "",
            last_use: "",
            disabled:true,
            all_keys:[]
        };
        this.getAPIkeys=this.getAPIkeys.bind(this);
    }


    async getAPIForDelete(id){
     var token=localStorage.getItem("access_token")
      var decoded=jwt_decode(token);
        var time_exp=decoded.exp;
        if(time_exp<new Date().getTime()/1000) {
            await refreshToken();
        }
        axios.delete(`http://localhost:5000/ipm/api/v0/api-keys?key_id=${id}`,
            {headers: {'Authorization': `Bearer ${localStorage.getItem('access_token')}`}}).then(res=>{
        return console.log(res)
    })
    }

    deleteAPIkey(){
        let idAPI = document.activeElement.value;
           this.state.all_keys.map(keys=>{
               if(keys.api_id==idAPI){
                   try{
                       this.getAPIForDelete(idAPI);
                       this.refreshApiKeys();
                       SuccessDeleteAPIkey()
                   }catch (e) {
                        ErrorDeleteAPIkey()
                   }
               }
        })
    }

   async getAPIkeys(){
     var token=localStorage.getItem("access_token")
      var decoded=jwt_decode(token);
        var time_exp=decoded.exp;
        if(time_exp<new Date().getTime()/1000) {
            await refreshToken();
        }
              axios.get("http://localhost:5000/ipm/api/v0/api-keys",{headers: {
    'Authorization': `Bearer ${localStorage.getItem("access_token")}`
        }}).then(res=>{
            this.setState({
                all_keys:res.data,
            }, ()=>
            console.log(this.state));
        });
    }

    refreshApiKeys(){
        this.getAPIkeys();
    }
    componentDidMount() {
        this.getAPIkeys()
    }
 render(){

        return(
           <div className="all_accounts">
               <div className="base-container" ref={this.props.containerRef}>
                    <div className="header" id="login">All API keys</div>
                    <div className="content">
                        <div className="image">
                            <img src={apiImg}/>
                        </div>
                        <div className="form">
                        <Slider duration={200}>
               {this.state.all_keys.map((keys,index)=>(
                   <div key={index} className="slider-content">
                       <div className="inner">
                           <div className="form-block">
                               <form>
                                   <input type="hidden" name="id" id="apikey-id"  value={keys.api_id}/>
                                <div className="form-groups">
                                    <input className="form-field" id="apikey-public" type="text" name="public" value={keys.api_key}  disabled = {(this.state.disabled)? "disabled" : ""}/>
                                    <label className="form-label" htmlFor="apikey-public"> Public key</label>
                                </div>
                                <div className="form-groups">
                                    <input className="form-field" id="apikey-created" type="text" name="created" value={keys.created}   disabled = {(this.state.disabled)? "disabled" : ""}/>
                                    <label className="form-label" htmlFor="apikey-created">Created at: </label>
                                </div>
                                <div className="form-groups">
                                    <input className="form-field" id="apikey-modified" type="text" name="modified" value={keys.last_use}  disabled = {(this.state.disabled)? "disabled" : ""}/>
                                    <label className="form-label" htmlFor="modified">Last use:</label>
                                </div>
                                   <button id="account-delete" type="button" value={keys.api_id} onClick={this.deleteAPIkey.bind(this)} className="btn">Delete</button>
                           </form>
                           </div>
                       </div>
                   </div>
                   ))}
                   </Slider>
                        </div>
                        </div>
                    </div>
                    <div className="footer">
                        <div className="message-dialog message-dialog-hide desktop-top-margin-big">
                            <div className="message-dialog-title">
                            </div>
                            <p className="message-dialog-content">
                            </p>
                        </div>
                    </div>
            </div>
        );
    }
}