import React, {Component} from "react";

import pwdImg from "./view_pass.svg";
import axios from 'axios';

import "./pwd.scss";
import "../accounts/all_acc_style.scss";
import "../slider.css";

import Slider from "react-animated-slider";


import {NavigationBar} from "../navigation/navigationBar";


export class View_passwords extends Component{
    constructor(props) {
        super(props);
         this.state = {
            id:"",
            name: "",
            password: "",
            host: "",
            description: "",
            disabled:true,
             searchText:"",
            all_account:[]
        };
          this.onChange = this.onChange.bind(this);
          this.getAccountsHost=this.getAccountsHost.bind(this)
    }

onChange(event){
        this.setState({[event.target.name]: event.target.value});
    }


    getAccounts(){
        let pass=localStorage.getItem("public")+":"+localStorage.getItem("secret");
              axios.get("http://localhost:5000/ipm/api/v0/api/passwords",{headers: {
    'X-API_Key': `${pass}`
        }}).then(res=>{
            this.setState({
                all_account:res.data,
            }, ()=>
            console.log(this.state));
        });
    }

     getAccountsHost(){

       let pass=localStorage.getItem("public")+":"+localStorage.getItem("secret");
        let host="";
        if(document.getElementById("show-host").value==""){
            host="";
        }else{
            host="?host="+document.getElementById("show-host").value;
        }
              axios.get(`http://localhost:5000/ipm/api/v0/api/passwords${host}`,{headers: {
    'X-API_Key': `${pass}`
        }}).then(res=>{
            this.setState({
                all_account:res.data,
            }, ()=>
            console.log(this.state));
        });
    }

    componentDidMount() {
        this.getAccounts()
    }


    render(){
    let links = [
      { label: 'Accounts', link: '/accounts'},
      { label: 'API key', link: '/apikey'},
      { label: 'Passwords', link: '/passwords',active:true  },
      { label: 'Log out', link: '/logout' },
    ];
        return(
              <div className="App">
                   <div id="navigation" className="container-center">
                 <NavigationBar links={links}  />
                </div>
           <div className="account">
               <div className="container">
           <div className="all_accounts">
               <div className="base-container" ref={this.props.containerRef}>
                    <div className="header" id="login">All accounts</div>
                    <div className="content">
                        <div className="image">
                            <img src={pwdImg}/>
                        </div>
                        <div className="form">
                        <Slider duration={200}>
               {this.state.all_account.map((account,index)=>(
                   <div key={index} className="slider-content">
                       <div className="inner">
                           <div className="form-block">
                               <form>

                                <div className="form-groups" >
                                    <input className="form-field" id="account-name" type="text" name="name" value={account.username} disabled = {(this.state.disabled)? "disabled" : ""}/>
                                    <label className="form-label" htmlFor="account-name"> Account E-mail</label>
                                </div>
                                <div className="form-groups">
                                    <input className="form-field" id="account-password" type="text" name="password" value={account.password}  disabled = {(this.state.disabled)? "disabled" : ""}/>
                                    <label className="form-label" htmlFor="password">Password</label>
                                </div>
                                <div className="form-groups">
                                    <input className="form-field" id="account-host" type="text" name="host" value={account.host}  disabled = {(this.state.disabled)? "disabled" : ""}/>
                                    <label className="form-label" htmlFor="host">Host</label>
                                </div>
                                <div className="form-groups">
                                    <input className="form-field" id="account-description" type="text" name="description" value={account.Description}  disabled = {(this.state.disabled)? "disabled" : ""}/>
                                    <label className="form-label" htmlFor="description">Description</label>
                                </div>
                           </form>
                           </div>
                       </div>
                   </div>
                   ))}
                   </Slider>
                             <div style={{position:"absolute",top:"20%",left:"100%"}} className="form-groups">
                                    <button style={{marginLeft:"20%"}} id="api-show" type="submit"  onClick={this.getAccountsHost} className="btn">Show accounts</button>
                                    <input style={{backgroundColor:"#8e998e",color:"whitesmoke"}} className="form-field" placeholder="Enter host:" onKeyUp={this.getAccountsHost} id="show-host" type="text"/>
                                    <label className="form-label" htmlFor="show-host">Enter host for search:</label>
                                </div>
                        </div>
                        </div>
                    </div>
                    <div className="footer">
                        <div className="message-dialog message-dialog-hide desktop-top-margin-big">
                            <div className="message-dialog-title">
                            </div>
                            <p className="message-dialog-content">
                            </p>
                        </div>
                    </div>
            </div>
               </div>
           </div>
              </div>
        );
    }
}