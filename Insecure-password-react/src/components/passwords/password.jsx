import React, {Component} from "react";

import pwdImg from "./all_pwd.svg"

import "./pwd.scss";
import "../accounts/all_acc_style.scss";
import "../accounts/style_acc.scss";

import {
    checkValidationApi,
    ErrorLoginApi,
    SuccessLoginApi
} from "../../functions/messages";


import history from "../../history";
import {loginApi} from "../../functions/userFucntion";
import {NavigationBar} from "../navigation/navigationBar";


export class Password extends Component{
    constructor(props) {
        super(props);
        this.state = {
            id:"",
            secret:"",
            api_password:"",
            all_account:[]
        };
        this.onSubmit=this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    onChange(event){
        this.setState({[event.target.name]: event.target.value});
    }

    onSubmit(event){
        event.preventDefault()
        const userApi={
            key_api:this.state.id,
            api_secret:this.state.secret
        }
        loginApi(userApi).then(res=>{
            if(res[1]===200){
              SuccessLoginApi();
              history.push('/api/passwords')
            }else{
               ErrorLoginApi()
            }
        }).catch(err=>{
             ErrorLoginApi()
        })
    }


    render() {
    let links = [
      { label: 'Accounts', link: '/accounts'},
      { label: 'API key', link: '/apikey' },
      { label: 'Passwords', link: '/passwords', active:true },
      { label: 'Log out', link: '/logout' },
    ];
        return(
             <div className="App">
                  <div id="navigation" className="container-center">
                 <NavigationBar links={links}  />
                </div>
           <div className="account">
               <div className="container">
            <div className="base-container" ref={this.props.containerRef}>
                <div className="header" id="login">Login with API key</div>
                <div className="content">
                    <div className="image">
                        <img src={pwdImg}/>
                    </div>
                    <div className="form">
                        <form onSubmit={this.onSubmit}>
                             <div className="form-group">
                                <input className="form-field" id="api-id" type="text" name="id" placeholder="Insert your public password" value={this.state.id} onChange={this.onChange} required/>
                                <label className="form-field" htmlFor="api-id">Public password</label>
                            </div>
                            <div className="form-group">
                                <input className="form-field" id="api-secret" type="text" name="secret"
                                       placeholder="Insert secret password" value={this.state.secret}
                                       onChange={this.onChange} required/>
                                <label className="form-label" htmlFor="api-secret">Secret password</label>
                            </div>
                            <button id="api-submit" type="submit" onClick={checkValidationApi} className="btn">Login</button>
                        </form>
                    </div>
                </div>
                <div className="footer">
                    <div className="message-dialog message-dialog-hide desktop-top-margin-big">
                        <div className="message-dialog-title">
                        </div>
                        <p className="message-dialog-content">
                        </p>
                    </div>
                </div>
            </div>
           </div>
             </div>
             </div>
        );
    }
}
export default Password;