import React, {Component} from "react";

import accImg from "./all_accounts.svg";
import axios from 'axios';

import "../slider.css";
import "../accounts/all_acc_style.scss";
import Slider from "react-animated-slider";

import {
    ErrorDeleteAccount,
    ErrorUpdateAccount,
    SuccessDeleteAccount,
    SuccessUpdateAccount
} from "../../functions/messages";
import jwt_decode from "jwt-decode";
import {refreshToken} from "../../functions/userFucntion";

export class View_accounts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id:"",
            name: "",
            password: "",
            host: "",
            description: "",
            disabled:true,
            all_account:[]
        };
        this.handleInputChange=this.handleInputChange.bind(this);
        this.getAccounts=this.getAccounts.bind(this);
    }


    async updateAccount(id,account){
        var token=localStorage.getItem("access_token")
      var decoded=jwt_decode(token);
        var time_exp=decoded.exp;
        if(time_exp<new Date().getTime()/1000) {
            await refreshToken();
        }
        axios.put( `http://localhost:5000/ipm/api/v0/accounts?account_id=${id}`, {
        acc_id:id,
        acc_name:account.email,
        acc_pass:account.password,
        acc_host:account.host,
        acc_description:account.description
    },{headers: {'Authorization': `Bearer ${localStorage.getItem('access_token')}`}}).then(res=>{
        return console.log(res)
    }).catch(err=>{
        console.log(err)
    })
    }
    refreshAllAccounts(){
        this.getAccounts()
    }
    async getAccountForDelete(id){
         var token=localStorage.getItem("access_token")
      var decoded=jwt_decode(token);
        var time_exp=decoded.exp;
        if(time_exp<new Date().getTime()/1000) {
            await refreshToken();
        }
        axios.delete(`http://localhost:5000/ipm/api/v0/accounts?account_id=${id}`,
            {headers: {'Authorization': `Bearer ${localStorage.getItem('access_token')}`}}).then(res=>{
        return console.log(res)
    })
    }

    deleteAccount(){
        let idAcc = document.activeElement.value;
           this.state.all_account.map(account=>{
               if(account.id==idAcc){
                   try{
                       this.getAccountForDelete(idAcc);
                       this.refreshAllAccounts();
                       SuccessDeleteAccount();
                   }catch (e) {
                        ErrorDeleteAccount()
                   }
               }
        })
    }

    changeDisabled(){
      this.setState({disabled:!this.state.disabled});
      if(this.state.disabled==true){
          document.getElementById("account-update").innerHTML="Update";
      }else{
          document.getElementById("account-update").innerHTML="Change";
          const acc={
            acc_id:"",
            acc_name:"",
            acc_pass:"",
            acc_host:"",
            acc_description:""
        };
           this.state.all_account.map(account=>{
               let idAcc = document.activeElement.value;
               if(account.id==idAcc){
                acc.id=account.id;
                acc.email=account.name;
                acc.password= account.password;
                acc.host=account.host;
                acc.description=account.description;
               }
        });
          try{
              this.updateAccount(acc.id,acc);
              this.refreshAllAccounts();
              SuccessUpdateAccount();
              document.getElementById("account-update").innerHTML="Change";
          }catch (e) {
                ErrorUpdateAccount()
          }
      }
    }


    async getAccounts(){
         var token=localStorage.getItem("access_token")
      var decoded=jwt_decode(token);
        var time_exp=decoded.exp;
        if(time_exp<new Date().getTime()/1000) {
            await refreshToken();
        }
              axios.get("http://localhost:5000/ipm/api/v0/accounts",{headers: {
    'Authorization': `Bearer ${localStorage.getItem("access_token")}`
        }}).then(res=>{
                this.setState({
                all_account:res.data,
            }, ()=>
            console.log(this.state));
        }).catch(e=>{
            console.log(e);
              })
    }

    componentDidMount() {
        this.getAccounts()
    }
////////////////////////////////////////
     saveValuesName(name){
        this.state.all_account.map(account=>{
            account.name=name;
        })
    }
     saveValuesPassword(password){
        this.state.all_account.map(account=>{
            account.password=password;
        })
    }
     saveValuesHost(host){
        this.state.all_account.map(account=>{
            account.host=host;
        })
    }
     saveValuesDescription(desc){
        this.state.all_account.map(account=>{
            account.description=desc;
        })
    }
//////////////////////////////

handleInputChange(e){
        const target=e.target;
        const value=target.value;
        const name=target.name;

        this.setState({
            [name]: value
        });
        if(name==="name"){
            this.saveValuesName(value);
        }else if(name==="password"){
            this.saveValuesPassword(value)
        }else if(name==="host"){
            this.saveValuesHost(value)
        }else if(name==="description"){
            this.saveValuesDescription(value)
        }
}

    render(){

        return(
           <div className="all_accounts">
               <div className="base-container" ref={this.props.containerRef}>
                    <div className="header" id="login">All accounts</div>
                    <div className="content">
                        <div className="image">
                            <img src={accImg}/>
                        </div>
                        <div className="form">
                        <Slider duration={200}>
               {this.state.all_account.map((account,index)=>(
                   <div key={index} className="slider-content">
                       <div className="inner">
                           <div className="form-block">
                               <form>
                                   <input type="hidden" name="id" id="account-id"  value={account.id}/>
                                <div className="form-groups">
                                    <input className="form-field" id="account-name" type="text" name="name" value={account.name} onChange={this.handleInputChange} disabled = {(this.state.disabled)? "disabled" : ""}/>
                                    <label className="form-label" htmlFor="account-name"> Account E-mail</label>
                                </div>
                                <div className="form-groups">
                                    <input className="form-field" id="account-password" type="text" name="password" value={account.password} onChange={this.handleInputChange}  disabled = {(this.state.disabled)? "disabled" : ""}/>
                                    <label className="form-label" htmlFor="password">Password</label>
                                </div>
                                <div className="form-groups">
                                    <input className="form-field" id="account-host" type="text" name="host" value={account.host} onChange={this.handleInputChange}  disabled = {(this.state.disabled)? "disabled" : ""}/>
                                    <label className="form-label" htmlFor="host">Host</label>
                                </div>
                                <div className="form-groups">
                                    <input className="form-field" id="account-description" type="text" name="description" value={account.description} onChange={this.handleInputChange}  disabled = {(this.state.disabled)? "disabled" : ""}/>
                                    <label className="form-label" htmlFor="description">Description</label>
                                </div>
                                   <button style={{marginRight:"2px"}} id="account-update" value={account.id} onClick={this.changeDisabled.bind(this)}  type="button"  className="btn">Change</button>
                                   <button id="account-delete" type="button" value={account.id} onClick={this.deleteAccount.bind(this)} className="btn">Delete</button>
                           </form>
                           </div>
                       </div>
                   </div>
                   ))}
                   </Slider>
                        </div>
                        <div className="message-dialog message-dialog-hide desktop-top-margin-big">
                            <div className="message-dialog-title">
                            </div>
                            <p className="message-dialog-content">
                            </p>
                        </div>
                        </div>
                    </div>

            </div>
        );
    }
}


