import React, {Component} from "react";
import "../accounts/style_acc.scss";

import "../../App.scss";
import accImg from "../accounts/add_account.svg";
import {checkAccount, ErrorCreateAccount, SuccessAccount} from "../../functions/messages";
import {add_account} from "../../functions/userFucntion";

export class Add_accounts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            password: "",
            host: "",
            description: ""
        };
         this.onSubmit=this.onSubmit.bind(this);
        this.onChange=this.onChange.bind(this);
    }


     onChange(event){
        this.setState({[event.target.name]: event.target.value});
    }

     onSubmit(event){
        event.preventDefault()
        const account={
            name:this.state.name,
            pass:this.state.password,
            host:this.state.host,
            description:this.state.description
        }
        add_account(account).then(res=>{
            if(res[1]===200){
             SuccessAccount()
            }else{
               ErrorCreateAccount()
            }
        }).catch(err=>{
            ErrorCreateAccount()
        })
    }

    render() {
        return (
            <div className="base-container" ref={this.props.containerRef}>
                    <div className="header" id="login">Add account</div>
                    <div className="content">
                        <div className="image">
                            <img src={accImg}/>
                        </div>
                        <div className="form">
                            <form noValidate onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <input className="form-field" id="account-name" type="text" name="name"
                                           placeholder="Insert your email" value={this.state.name}
                                           onChange={this.onChange}
                                           required/>
                                    <label className="form-label" htmlFor="account-name"> Account
                                        E-mail</label>
                                </div>
                                <div className="form-group">
                                    <input className="form-field" id="account-password" type="text" name="password"
                                           placeholder="Insert password" value={this.state.password}
                                           onChange={this.onChange} required/>
                                    <label className="form-label" htmlFor="password">Password</label>
                                </div>
                                <div className="form-group">
                                    <input className="form-field" id="account-host" type="text" name="host"
                                           placeholder="Insert host" value={this.state.host}
                                           onChange={this.onChange} required/>
                                    <label className="form-label" htmlFor="host">Host</label>
                                </div>
                                <div className="form-group">
                                    <input className="form-field" id="account-description" type="text"
                                           name="description"
                                           placeholder="Insert description" value={this.state.description}
                                           onChange={this.onChange}/>
                                    <label className="form-label" htmlFor="description">Description</label>
                                </div>
                                  <button id="account-submit" type="submit" onClick={checkAccount}
                                        className="btn">Add account</button>
                            </form>
                        </div>
                    </div>
                    <div className="footer">
                        <div className="message-dialog message-dialog-hide desktop-top-margin-big">
                            <div className="message-dialog-title">
                            </div>
                            <p className="message-dialog-content">
                            </p>
                        </div>
                    </div>
            </div>
        );
    }
}