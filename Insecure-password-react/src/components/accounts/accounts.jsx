import React, {Component} from "react";

import {Add_accounts} from "./add_accounts";
import {View_accounts} from "./view_accounts";
import {NavigationBar} from "../navigation/navigationBar"


export class Accounts extends Component{
constructor(props) {
    super(props);
    this.state={
      viewAll: true,
    };
  }
  componentDidMount() {
    this.rightSide.classList.add("right");
    }

  changeState(){
    const{viewAll}=this.state;

    if(viewAll){
      this.rightSide.classList.remove("right");
      this.rightSide.classList.add("left");
    }else{
      this.rightSide.classList.remove("left");
      this.rightSide.classList.add("right");
    }
    this.setState(prevState=>({viewAll: !prevState.viewAll}));
  }

  render(){
     let links = [
      { label: 'Accounts', link: '/accounts', active: true },
      { label: 'API key', link: '/apikey' },
      { label: 'Passwords', link: '/passwords' },
      { label: 'Log out', link: '/logout' },
    ];

    const { viewAll } = this.state;
    const current=viewAll ? "Add new account" : "View accounts";
    const currentActive=viewAll ? "add" : "view";
    return (
          <div className="App">
              <div id="navigation" className="container-center">
                 <NavigationBar links={links}  />
                </div>
           <div className="account">
             <div className="container" ref={ref => (this.container = ref)}>
            {viewAll && (<View_accounts containerRef={(ref) => this.current = ref}/>)}
            {!viewAll && (<Add_accounts containerRef={(ref) => this.current = ref}/>)}
             </div>
          <RightSide id="rightSide" current={current} currentActive={currentActive} containerRef={ref=>this.rightSide=ref} onClick={this.changeState.bind(this)}/>
        </div>
      </div>

    );
  }
}

export const RightSide = props => {
  return <div className="right-side" ref={props.containerRef} onClick={props.onClick}>
          <div className="inner-container">
          <div className="text">{props.current}</div>
      </div>
    </div>
}
