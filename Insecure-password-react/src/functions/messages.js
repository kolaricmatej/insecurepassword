import React from "react";

export const showMessageDialog = (title, content, type = 'default', autoCloseDelay = 0, buttons = [])=> {


    document.getElementsByClassName('message-dialog')[0].classList.remove("success-dialog", "warning-dialog", "error-dialog", "question-dialog", "info-dialog")


    switch (type) {
        case 'success':
            document.getElementsByClassName('message-dialog')[0].classList.add("success-dialog")
            break;
        case 'error':
            document.getElementsByClassName('message-dialog')[0].classList.add("error-dialog")
            break;
        case 'warning':
            document.getElementsByClassName('message-dialog')[0].classList.add("warning-dialog")
            break;
        case 'question':
            document.getElementsByClassName('message-dialog')[0].classList.add("question-dialog")
            break;
        case 'info':
            document.getElementsByClassName('message-dialog')[0].classList.add("info-dialog")
            break;
        default:
            break;
    }

    // title
    document.getElementsByClassName('message-dialog-title')[0].innerHTML =
    document.getElementsByClassName('message-dialog-title')[0].innerHTML =
        `
    <i class="ms-Icon message-dialog-icon" aria-hidden="true"></i>
    <p>${title}</p>
    <button id="message-dialog-close-btn"><i class="ms-Icon ms-Icon--ChromeClose" aria-hidden="true"></i></button>

    `

    // content
    document.getElementsByClassName('message-dialog-content')[0].innerHTML = content

    // show dialog

    document.getElementsByClassName('message-dialog')[0].classList.add("message-dialog-show")
    document.getElementsByClassName('message-dialog')[0].classList.remove("message-dialog-hide")

    // events
    document.getElementById('message-dialog-close-btn').addEventListener('click', hideMessageDialog, false)

    // auto-close
    if (autoCloseDelay > 0) {
        setTimeout(() => {
            hideMessageDialog()
        }, autoCloseDelay * 1000)
    }
}

export const hideMessageDialog=()=> {
    document.getElementsByClassName('message-dialog')[0].classList.add("message-dialog-hide");
    document.getElementsByClassName('message-dialog')[0].classList.remove("message-dialog-show");
}


export const loginCookie=(event)=>{
    if (!doesCookieExist("prviDolazak")) {
        showMessageDialog('Info', '🍪 This website uses cookies in order to offer you the most relevant information.', 'info')

        let currentDate = new Date()
        currentDate.setTime(currentDate.getTime() + 86400000)

        document.cookie = `prviDolazak=true; expires=${currentDate.toUTCString()}`;
    }
}

export const checkValidation=(event)=>{
    let isValid=true;
    let errorText="";

    if(document.getElementById("login-email").value.length<1){
        isValid=false;
        document.getElementById("login-email").classList.add('form-error-highlight')
        errorText+='You should insert your <code>E-mail address</code><br>'

    }else{
         document.getElementById("login-email").classList.remove('form-error-highlight');
    }

    if(document.getElementById("login-password").value.length<1){
        isValid=false;
        document.getElementById("login-password").classList.add('form-error-highlight')
        errorText+='You should insert your <code>password</code><br>'

    }else{
         document.getElementById("login-password").classList.remove('form-error-highlight');
    }

    if(!isValid){
        showMessageDialog("Error",errorText,'error')
        event.preventDefault()
    }


    return
}
export const successLogin=()=>{
     showMessageDialog('Success', "You are successfully log in",'success',2)
}
export const incorrectLogin=()=>{
    showMessageDialog("Warning","Wrong password or E-mail",'warning')
}

export const checkRegistration=(event)=>{
    let isValid=true;
    let errorText="";

     if (document.getElementById("registration-email").value.length<1) {
        isValid = false;
        document.getElementById("registration-email").classList.add("form-error-highlight");
        errorText += 'You should insert your <code>E-mail address</code><br>'
        }else{
        document.getElementById("registration-email").classList.remove("form-error-highlight");
        }

    if (document.getElementById("registration-password").value.length<1) {
        isValid = false;
        document.getElementById("registration-password").classList.add("form-error-highlight");
        errorText += 'You should insert your <code>password</code><br>'
    }else{
        document.getElementById("registration-password").classList.remove("form-error-highlight");
        }

     if (document.getElementById("registration-repeat").value.length<1) {
         isValid = false;
         document.getElementById("registration-repeat").classList.add("form-error-highlight");
         errorText += 'You should insert your <code>password</code> again<br>'
     }else{
        document.getElementById("registration-repeat").classList.remove("form-error-highlight");
        }

    if(!isValid){
        showMessageDialog("Error",errorText,'error')
        event.preventDefault()
    }
    return

}

export const SuccessRegistration=(event)=>{
    showMessageDialog('Success', "You are successfully registered in",'success',1)
}
export const ErrorRegistration=(event)=>{
    showMessageDialog('Error', "Error occurred while trying to create a user",'info',2)
}
export const ErrorEmailExists=(event)=>{
    showMessageDialog('Error', "User with that E-mail already exists! Please use another E-mail",'error')
}
export const checkEmail=(event)=>{
    let isValid=true;
    let errorText="";
    let email=document.getElementById("registration-email").value;
     if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
        isValid = false;
        document.getElementById("registration-email").classList.remove("form-success-highlight");
        document.getElementById("registration-email").classList.add("form-error-highlight");
        errorText += 'You should insert valid <code>E-mail address</code><br>'
    }else{
        document.getElementById("registration-email").classList.remove("form-error-highlight");
        document.getElementById("registration-email").classList.add("form-success-highlight");
        }
     if(!isValid){
        showMessageDialog("Error",errorText,'error')
        event.preventDefault()
    }
    return
}

export const checkPassword=(event)=>{
    let isValid=true;
    let errorText="";

     if(document.getElementById("registration-password").value!==document.getElementById("registration-repeat").value){
        isValid=false;
        document.getElementById("registration-repeat").classList.remove("form-success-highlight");
        document.getElementById("registration-repeat").classList.add("form-error-highlight");
         errorText += '<code>Passwords</code> are not same <br>'
     }else{
          document.getElementById("registration-repeat").classList.remove("form-error-highlight");
          document.getElementById("registration-repeat").classList.add("form-success-highlight");
          hideMessageDialog()
     }
      if(!isValid){
        showMessageDialog("Error",errorText,'error')
        event.preventDefault()
    }
}


export const checkAccount=(event)=>{
    let isValid=true;
    let errorText="";


     if (document.getElementById("account-name").value.length<1) {
        isValid = false;
        document.getElementById("account-name").classList.add("form-error-highlight");
        errorText += 'You should insert your <code>E-mail address</code><br>'
        }else{
        document.getElementById("account-name").classList.remove("form-error-highlight");
        }

    if (document.getElementById("account-password").value.length<1) {
        isValid = false;
        document.getElementById("account-password").classList.add("form-error-highlight");
        errorText += 'You should insert your <code>password</code><br>'
    }else{
        document.getElementById("account-password").classList.remove("form-error-highlight");
        }

     if (document.getElementById("account-host").value.length<1) {
         isValid = false;
         document.getElementById("account-host").classList.add("form-error-highlight");
         errorText += 'You should insert your <code>host</code><br>'
     }else{
        document.getElementById("account-host").classList.remove("form-error-highlight");
        }

    if(!isValid){
        showMessageDialog("Error",errorText,'error')
        event.preventDefault()
    }
    return

}

export const SuccessAccount=(event)=>{
        showMessageDialog('Success', "You have successfully added new account",'success',1)
    }
export const ErrorCreateAccount=(event)=>{
        showMessageDialog('Error', "There was a mistake while creating account please try again",'error')
    }

export const ErrorUpdateAccount=(event)=>{
    showMessageDialog('Error', "There was a mistake while updating account! Please try again.",'info')
}
export const SuccessUpdateAccount=(event)=>{
    showMessageDialog('Notification', "You have successfully update account",'info')
}
export const SuccessDeleteAccount=(event)=>{
    showMessageDialog('Notification', "You have successfully deleted account",'info')
}

export const ErrorDeleteAccount=(event)=>{
    showMessageDialog('Error', "There was a mistake while deleting account! Please try again.",'error')
}

export const SecretKeyMessage=(secret)=>{
    showMessageDialog('Information', "Your secret key is: "+secret+" <br>This password will never be shown again",'info')
}

export const ErrorApiKey=(event)=>{
    showMessageDialog('Error', "There was a mistake while creating API key please try again",'error')
}

export const SuccessDeleteAPIkey=(event)=>{
    showMessageDialog('Notification', "You have successfully deleted API key",'info')
}
export const ErrorDeleteAPIkey=(event)=>{
    showMessageDialog('Error', "There was a mistake while deleting API key! Please try again.",'error')
}


export const checkValidationApi=(event)=>{
    let isValid=true;
    let errorText="";


    if(document.getElementById("api-id").value.length<1){
        isValid=false;
        document.getElementById("api-id").classList.add('form-error-highlight')
        errorText+='You should insert your <code>public password</code><br>'

    }else{
         document.getElementById("api-id").classList.remove('form-error-highlight');
    }

    if(document.getElementById("api-secret").value.length<1){
        isValid=false;
        document.getElementById("api-secret").classList.add('form-error-highlight')
        errorText+='You should insert your <code>secret password</code><br>'

    }else{
         document.getElementById("api-secret").classList.remove('form-error-highlight');
    }

    if(!isValid){
        showMessageDialog("Error",errorText,'error')
        event.preventDefault()
    }
    return
}

export const SuccessLoginApi=(event)=>{
        showMessageDialog('Success', "You are successfully logged in",'success',1)
    }

export const ErrorLoginApi=(event)=>{
        showMessageDialog('Error', "There was a mistake while logging on account please try again",'error')
    }

export const LogoutMessage=(event)=>{
    showMessageDialog('Success', "You are successfully logged out!",'success', 2)
}

function doesCookieExist(name) {
    if (document.cookie.indexOf(name + '=') > -1) return true
    else return false
}

export const RefreshTokenExpired=(event)=>{
    showMessageDialog('Info', "Your refresh token has expired! Please log in again!",'info', 2)
}
export const AccessTokenExpired=(event)=>{
    showMessageDialog('Warning', "Your access token has expired! Please refresh page!",'warning')
}