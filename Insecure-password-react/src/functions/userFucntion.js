import axios from 'axios'
import {AccessTokenExpired, RefreshTokenExpired} from "./messages";
import history from "../history";
import jwt_decode from "jwt-decode";

axios.interceptors.response.use(function (response) {
    return response;

},function (error) {
    if(401===error.response.status){
        if(error.response.data['msg'].includes("refresh")){
            RefreshTokenExpired();
            history.push("/")
        }else if(error.response.data['msg'].includes("revoked")){
            history.push("/")
        }else{
            AccessTokenExpired();
            refreshToken();
        }
    }
    return Promise.reject(error)
});


export const login = user=>{
    return axios.post('http://localhost:5000/ipm/api/v0/login',{
        email:user.email,
        password:user.password
    }).then(res=>{
        localStorage.setItem('access_token',res.data[0]['access_token']);
        localStorage.setItem('refresh_token',res.data[0]['refresh_token']);
        return res.data
    }).catch(err=>{
        console.log(err)
    })
}

export const registration = newUser=>{
    return axios.post('http://localhost:5000/ipm/api/v0/registration',{
        first_name:newUser.first_name,
        last_name:newUser.lastName,
        email:newUser.email,
        password:newUser.password
    }).then(res=>{
        return res.data
        })
}

export const add_account = async account=>{
    var token=localStorage.getItem("access_token")
      var decoded=jwt_decode(token);
        var time_exp=decoded.exp;
        if(time_exp<new Date().getTime()/1000) {
            await refreshToken();
        }
    return axios.post("http://localhost:5000/ipm/api/v0/accounts",{
        acc_name:account.name,
        acc_pass:account.pass,
        acc_host:account.host,
        acc_description:account.description
   }, {headers: {
    'Authorization': `Bearer ${localStorage.getItem('access_token')}`
        }}).then(res=>{
        return res.data
    })
}

export const add_apikey=async ()=>{
     var token=localStorage.getItem("access_token")
      var decoded=jwt_decode(token);
        var time_exp=decoded.exp;
        if(time_exp<new Date().getTime()/1000) {
            await refreshToken();
        }
    return axios.post("http://localhost:5000/ipm/api/v0/api-keys",null,{headers: {
    'Authorization': `Bearer ${localStorage.getItem('access_token')}`
        }}).then(res=>{
        return res.data
    })
}

export const loginApi=userApi=>{
    let pass=document.getElementById("api-id").value+":"+document.getElementById("api-secret").value;
    return axios.post("http://localhost:5000/ipm/api/v0/passwords",{
        key_api:userApi.key_api,
        api_secret:userApi.api_secret
    },{headers: {
    'Authorization': `Bearer ${pass}`
        }}).then(res=>{
             localStorage.setItem('public',res.data[0]['key_api']);
             localStorage.setItem('secret',res.data[0]['api_secret']);
            return res.data
    })
}



export const refreshToken=()=>{
    return axios.post("http://localhost:5000/ipm/api/v0/refresh",null,{headers: {
    'Authorization': `Bearer ${localStorage.getItem("refresh_token")}`
        }}).then(res=>{
            localStorage.setItem('access_token',res.data['access_token']);
            return res.data;
    }).catch(e=>{
        console.log(e)
    })
}
