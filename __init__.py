import redis
from flask import Flask
from flask_jwt_extended import JWTManager
from flask_restplus import Api

from .database.db import initialize_db
from flask_cors import CORS
jwt = JWTManager()



def create_app(config):
    app = Flask(__name__)
    app.config.from_object(config)
    store = redis.StrictRedis(host='localhost', port=6379, db=0, decode_responses=True)
    app.store = store
    store.set
    CORS(app)
    initialize_db(app)

    jwt.init_app(app)


    from .api import create_api
    create_api(app)

    return app
