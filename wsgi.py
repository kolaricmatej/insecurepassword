from . import create_app
from .config import configs


app = create_app(configs['default'])

if __name__ == '__main__':
    app.run()
