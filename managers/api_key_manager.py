import json
import os
import secrets

from flask import jsonify
from flask_jwt_extended import get_jwt_identity

from ..database.models import ApiKey


class ManageApiKey:
    @staticmethod
    def createApiKey():
        api_key = ApiKey()
        api_key.api_user = get_jwt_identity()['user_id']
        api_key.api_secret = secrets.token_urlsafe(4)
        visible_key = api_key.api_secret
        api_key.hash_secret()
        api_key.key_api = os.urandom(16).hex()
        api_key.save()
        return jsonify({
            'key': api_key.key_api,
            'secret': visible_key,
            'date': api_key.created_at
        }, 200)

    @staticmethod
    def getAll():
        api_key = ApiKey()
        api_key.api_user = get_jwt_identity()['user_id']
        all_api = ApiKey.objects(api_user=api_key.api_user)
        key_list = []
        for x in range(0, all_api.count()):
            key_list.append({
                "api_id": all_api[x]["api_id"],
                "api_key": all_api[x]['key_api'],
                "created": all_api[x]['created_at'],
                "last_use": all_api[x]['modified_at']
            })
        return jsonify(key_list)

    @staticmethod
    def deleteApiKey(key_id):
        existing_key = ApiKey.objects(api_id=key_id).first()
        if existing_key is not None:
            existing_key.delete()
            return {'message': 'OK'}, 200
        else:
            return {'message': 'ERR'}, 400
