from flask_jwt_extended import create_access_token, create_refresh_token, get_jti, get_raw_jwt, get_jwt_identity
from flask import request, current_app, jsonify

from .. import jwt
from ..config import Config
from ..database.models import AuthUser


class AuthManager:
    @staticmethod
    def registration():
        body = request.get_json()
        existing_user = AuthUser.objects(email=body.get('email')).first()
        if existing_user:
            return jsonify({
                'message': 'User with the same username already exists',
            }, 409)
        user = AuthUser(**body)
        user.generate_uuid()
        user.hash_password()
        try:
            user.save()
            return jsonify({
                'message': 'OK',
            }, 200)
        except Exception as ex:
            print('Unexpected exception occurred while creating a user: %s', ex)
            return jsonify({
                'message': 'Error occurred while trying to create a user',
            }, 500)

    @staticmethod
    def login():
        body = request.get_json()
        auth_user = AuthUser.objects.get(email=body.get('email'))
        password = auth_user.check_password(body.get('password'))
        if password:
            access_token = create_access_token(identity={'email': auth_user['email'],
                                                         'first_name': auth_user['first_name'],
                                                         'last_name': auth_user['last_name'],
                                                         'user_id': auth_user['user_id']})
            refresh_token = create_refresh_token(identity={'email': auth_user['email'],
                                                           'first_name': auth_user['first_name'],
                                                           'last_name': auth_user['last_name'],
                                                           'user_id': auth_user['user_id']})
            access_jti = get_jti(encoded_token=access_token)
            refresh_jti = get_jti(encoded_token=refresh_token)

            current_app.store.set(access_jti, 'false', (Config.JWT_ACCESS_TOKEN_EXPIRES * 1.2))
            current_app.store.set(refresh_jti, 'false', Config.JWT_REFRESH_TOKEN_EXPIRES * 1.2)

            return jsonify({
                'access_token': access_token,
                'refresh_token': refresh_token
            }, 200)
        return jsonify({'error': 'Email or password are incorrect! Please try again'}, 401)

    @staticmethod
    def logoutAccess():
        jti = get_raw_jwt()['jti']
        current_app.store.set(jti, 'true', Config.JWT_ACCESS_TOKEN_EXPIRES * 1.2)
        return {"message": "Access token revoked"}, 200

    @staticmethod
    def logoutRefresh():
        jti = get_raw_jwt()['jti']
        current_app.store.set(jti, 'true', Config.JWT_REFRESH_TOKEN_EXPIRES * 1.2)
        return {"message": "Refresh token revoked"}, 200

    @staticmethod
    def tokenRefresh():
        current_user = get_jwt_identity()
        access_token = create_access_token(identity=current_user)
        access_jti = get_jti(encoded_token=access_token)
        current_app.store.set(access_jti, 'false', Config.JWT_ACCESS_TOKEN_EXPIRES * 1.2)
        return {'access_token': access_token}


@jwt.expired_token_loader
def my_expired_token_callback(expired_token):
    token_type = expired_token['type']
    return jsonify({
        'status': 401,
        'sub_status': 42,
        'msg': 'The {} token has expired'.format(token_type)
    }), 401

