from datetime import datetime
import time
from flask import request, jsonify
from flask_jwt_extended import get_jwt_identity, get_raw_jwt

from .auth_manager import AuthManager
from ..database.models import Account


class ModifyUser:
    @staticmethod
    def addAccount():
        body = request.get_json()
        user = Account(**body)
        user.acc_user = get_jwt_identity()['user_id']
        user.save()
        return jsonify({
            'message': 'OK',
        }, 200)

    @staticmethod
    def showAll():
        user = Account()
        user.acc_user = get_jwt_identity()['user_id']
        all_acc = Account.objects(acc_user=user.acc_user)
        acc_list = []
        for x in range(0, all_acc.count()):
            acc_list.append({
                "id": all_acc[x]['acc_id'],
                "name": all_acc[x]['acc_name'],
                "password": all_acc[x]['acc_pass'],
                "host": all_acc[x]['acc_host'],
                "description": all_acc[x]['acc_description']
            })
        return jsonify(acc_list)

    @staticmethod
    def updateAccount(account_id):
        body = request.get_json()
        existing_acc = Account.objects(acc_id=account_id).first()
        if existing_acc is not None:
            user = Account(**body)
            existing_acc.acc_name = user.acc_name
            existing_acc.acc_pass = user.acc_pass
            existing_acc.acc_host = user.acc_host
            existing_acc.acc_description = user.acc_description
            user.acc_user = get_jwt_identity()['user_id']
            existing_acc.acc_user = user.acc_user
            existing_acc.save()
            return {'message': 'OK'}, 200
        return {'message': 'ERR'}, 400

    @staticmethod
    def deleteAccount(account_id):
        existing_acc = Account.objects(acc_id=account_id).first()
        if existing_acc is not None:
            existing_acc.delete()
            return jsonify({
                'message': 'OK',
            }, 200)
        else:
            return {'message': 'ERR'}, 400
