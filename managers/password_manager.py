from flask import request, jsonify
from mongoengine import Q

from ..database.models import Account, ApiKey


class PasswordManagerAPI:
    def showAccounts(user):
        host = request.args.get('host')
        key_list = []
        all_accounts = Account.objects(acc_user=user['api_user'])
        if host is None:
            for x in range(0, all_accounts.count()):
                key_list.append({
                    "username": all_accounts[x]['acc_name'],
                    "password": all_accounts[x]['acc_pass'],
                    "host": all_accounts[x]['acc_host'],
                    "Description": all_accounts[x]['acc_description']
                })
            return jsonify(key_list)
        else:
            all_host = Account.objects(Q(acc_host=host) & Q(acc_user=user['api_user']))
            for x in range(0, all_host.count()):
                key_list.append({
                    "username": all_host[x]['acc_name'],
                    "password": all_host[x]['acc_pass'],
                    "host": all_host[x]['acc_host'],
                    "Description": all_host[x]['acc_description']
                })
            return jsonify(key_list)

    @staticmethod
    def loginApi():
        body = request.get_json()
        user = ApiKey.objects.get(key_api=body.get('key_api'))
        if user.verify_secret(body.get('api_secret')):
            return jsonify({
                'key_api': body.get('key_api'),
                'api_secret': body.get('api_secret')
            }, 200)
        else:
            return jsonify({'error': 'Email or password are incorrect! Please try again'}, 401)
