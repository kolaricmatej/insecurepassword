from flask_restplus import Api
from .namespaces import ns_ipm
from .. import jwt

def create_api(app):
    api = Api(
        app,
        version='1.0',
        title='Insecure Password Manager',
        description='Tool for storing passwords on one place'
    )
    jwt._set_error_handler_callbacks(api)
    from .routes import auth
    from .routes import account
    from .routes import api_key
    from .routes import passwords

    api.add_namespace(ns_ipm)
