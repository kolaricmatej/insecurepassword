from flask_restplus import Namespace

ns_ipm = Namespace('Password Keeper', path='/ipm/api/v0', description='Password manager actions')