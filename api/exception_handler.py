from flask_jwt_extended.exceptions import JWTDecodeError

from .namespaces import ns_ipm


@ns_ipm.errorhandler(JWTDecodeError)
def handle_expired_error(e):
    return {'message': 'Token has expired'}, 401
