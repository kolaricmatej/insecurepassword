from flask import request
from flask_jwt_extended import jwt_required
from flask_restplus import Resource

from ...managers.api_key_manager import ManageApiKey
from ..namespaces import ns_ipm


@ns_ipm.route('/api-keys')
class CreateApiKey(Resource):
    @jwt_required
    def post(self):
        return ManageApiKey.createApiKey()

    @jwt_required
    def get(self):
        return ManageApiKey.getAll()


@ns_ipm.route('/api-keys')
class DeleteApiKey(Resource):
    @jwt_required
    def delete(self):
        key_id=request.args.get('key_id')
        return ManageApiKey.deleteApiKey(key_id)

