from flask import request
from flask_jwt_extended import (jwt_required)
from flask_restplus import Resource

from ...managers.account_manager import ModifyUser
from ..namespaces import ns_ipm


@ns_ipm.route('/accounts')
class CreateAccountAPI(Resource):
    @jwt_required
    def post(self):
        return ModifyUser.addAccount()


@ns_ipm.route('/accounts')
class EditAccountAPI(Resource):
    @jwt_required
    def put(self):
        account_id = request.args.get('account_id')
        return ModifyUser.updateAccount(account_id)

    @jwt_required
    def delete(self):
        account_id = request.args.get('account_id')
        return ModifyUser.deleteAccount(account_id)

    @jwt_required
    def get(self):
        return ModifyUser.showAll()

