from datetime import datetime

from flask import request
from flask_restplus import Resource

from ...database.models import ApiKey
from ...managers.password_manager import PasswordManagerAPI
from ..namespaces import ns_ipm


def require_appkey(function):
    def wrapper(*args):
        api_key = request.environ['HTTP_X_API_KEY']
        api_key_format = api_key.split(":")
        key = api_key_format[0]
        secret = api_key_format[1]
        user = ApiKey.objects.get(key_api=key)
        if user.verify_secret(secret):
            user['modified_at'] = datetime.now()
            user.save()
            return function(*args, user)
        else:
            return {"Error": "Wrong api key or secret"}, 400

    return wrapper


@ns_ipm.route('/api/passwords')
class ShowAccountsAPI(Resource):
    @require_appkey
    def get(self, user):
        return PasswordManagerAPI.showAccounts(user)


@ns_ipm.route('/passwords')
class ShowAccountsAPI(Resource):
    @staticmethod
    def post():
        return PasswordManagerAPI.loginApi()