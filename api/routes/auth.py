from flask import current_app, jsonify
from flask_jwt_extended import jwt_required, jwt_refresh_token_required
from flask_restplus import Resource
from ...managers.auth_manager import AuthManager
from ..namespaces import ns_ipm
from ... import jwt

@ns_ipm.route('/login')
class Login(Resource):
    @staticmethod
    def post():
        return AuthManager.login()


@ns_ipm.route('/logout/access_revoke')
class LogoutAccess(Resource):
    @jwt_required
    def get(self):
        return AuthManager.logoutAccess()


@ns_ipm.route('/logout/refresh_revoke')
class LogoutRefresh(Resource):
    @jwt_refresh_token_required
    def get(self):
        return AuthManager.logoutRefresh()


@ns_ipm.route('/registration')
class Registration(Resource):
    @staticmethod
    def post():
        return AuthManager.registration()


@ns_ipm.route('/test_access')
class TestAccessAPI(Resource):
    @jwt_required
    def get(self):
        return {'message': 'OK'}, 200


@ns_ipm.route('/refresh')
class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        return AuthManager.tokenRefresh()


@jwt.token_in_blacklist_loader
def check(decrypted_token):
    jti = decrypted_token['jti']
    is_revoked = current_app.store.get(jti)
    if is_revoked is not None and is_revoked == 'false':
        return False
    return True



