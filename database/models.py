from datetime import datetime
from uuid import uuid4
from flask_bcrypt import generate_password_hash, check_password_hash
from mongoengine.fields import UUIDField, StringField, ListField, ReferenceField, SequenceField, DateTimeField
from .db import db


class Account(db.Document):
    acc_id = SequenceField(required=True)
    acc_name = StringField(required=True)
    acc_pass = StringField(required=True)
    acc_host = StringField(required=True)
    acc_description = StringField()
    acc_user = StringField(required=True)


class AuthUser(db.Document):
    user_id = UUIDField(primary_key=True)
    first_name = StringField(required=False, null=True)
    last_name = StringField(required=False, null=True)
    email = StringField(required=True, unique=True)
    password = StringField(required=True)
    accounts = ListField(ReferenceField(Account), required=False, null=True)

    def generate_uuid(self):
        self.user_id = str(uuid4())

    def hash_password(self):
        self.password = generate_password_hash(self.password).decode('utf8')

    def check_password(self, password):
        return check_password_hash(self.password, password)


class ApiKey(db.Document):
    api_id = SequenceField(required=True)
    key_api = StringField(required=True, unique=True)
    api_secret = StringField(required=True)
    api_user = StringField(required=True)
    created_at = DateTimeField(default=datetime.now())
    modified_at = DateTimeField(default=datetime.now())

    def myconverter(o):
        if isinstance(o, datetime):
            return o.__str__()

    def hash_secret(self):
        self.api_secret = generate_password_hash(self.api_secret).decode('utf8')

    def verify_secret(self, api_secret):
        return check_password_hash(self.api_secret, api_secret)