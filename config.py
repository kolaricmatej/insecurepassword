import os
from datetime import timedelta


class Config:
    MONGO_URI = os.getenv('MONGO_URI') or 'mongodb://localhost:27017/passwordDb'
    JWT_SECRET_KEY = os.getenv('JWT_SECRET_KEY') or '2Q3ZKCgHJCwgMo9sfsmO9Q'
    JWT_ACCESS_TOKEN_EXPIRES = os.getenv('JWT_ACCESS_TOKEN_EXPIRES') or timedelta(hours=1)
    JWT_REFRESH_TOKEN_EXPIRES = os.getenv('JWT_REFRESH_TOKEN_EXPIRES') or timedelta(days=30)
    JWT_BLACKLIST_ENABLED = os.getenv('JWT_BLACKLIST_ENABLED') or True
    JWT_BLACKLIST_TOKEN_CHECK = os.getenv('JWT_BLACKLIST_TOKEN_CHECKS') or ['access', 'refresh']


class DevConfig(Config):
    pass


class TestConfig(Config):
    pass


class ProdConfig(Config):
    pass


configs = {
    'default': Config,
    'dev': DevConfig,
    'test': TestConfig,
    'prod': ProdConfig
}
