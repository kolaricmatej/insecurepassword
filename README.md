# Insecure password Manager
#### This is user manual in which you can find all instructions about aplication and how you can use it. This aplication is called Insecure Password Manager and it purpose is managing with all passwords from your accounts. You can store all password on one place, update them and if some password are become useless you can delete it.

---
### How to run it?

First you need to go to directory insecure password react and in terminal you have to write **npm install** to install all dependencies. After that in same terminal write **yarn start** to ran react script. When you do this go to another terminal and in the same directory as previous write **yarn start-api**. This enebles you to use backend service on flask running on different port.


---
## 1) Login / Regisration 

When aplication is started if you dont have account on this aplication you shoud click on Registration on right side of screen which open registration form. On that form you should insert your valid email, password and other personal information. When you fill out all fields, click on registration button. After you click on registration button it redirect you to login form. On login form you should insert email and password that you typed in registration form. If you typed wrong password or email, you will recieve error message. If you checked box with title "Remember me ", your email and password will be saved for every other login. In case of valid login you will be redirected to home menu which shows you all accounts and feature to add new account if you doesn't have any or just want to add new one.

## 2) Account Manager

On this form you can see all of your accounts and add new one by clicking on button on right side of this form. If you want to add new account you have to fill form with information about account you want to save. When you fill out all fileds click on button add account. If there is no error message you are successfuly add account and you can see it if you click on the left button "All accounts". On that screen you can see all accounts and you can update them by clicking on button change, and delete account by clicking on button delete.


## 3) API key generator

This is part on which you can generate your api key that you can use to log in on Password API.
By click on the button generate api key it will show you public key, secret key and creation date. Public key is always accessible, while secret key is shown in message box and if you doesn't remeber it you will have to generate new api key. All of your api keys are visible by clicking on button all api keys on the left side of screen, and in that part you can delete it or see other information of api keys such as date of last use.

## 4)Passwords

This is part in which you use your api key that you created in prevous step. When you enterd your public key and secret key you gain access to all accounts and you can search them by the host.

## 5)Logout 

Clicking on logout on the navigation bar you will bi sing out from the aplication and you will be redirected to the log in page.

---
Possible errors:

* if you start react part of aplication and doesn't start backend part you will always receive message that your email or password are incorrect because it can't check it from base.
* check if you have redis installed. If you run it on Windows download **[redis service for Windows] (https://github.com/rgl/redis/downloads)** .


